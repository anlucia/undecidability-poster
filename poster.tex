\documentclass[a1paper, 17pt, portrait, margin=0mm, innermargin=15mm, blockverticalspace=5mm, colspace=8mm]{tikzposter}

\usepackage{lmodern}
\usepackage{fontspec}
%\setsansfont{Liberation Sans}
\setsansfont[Extension = .ttf, Path = ./fonts/,  UprightFont = *-Regular,
ItalicFont = *-Italic, BoldFont = *-Bold, BoldItalicFont = *-BoldItalic, SmallCapsFont=*-Regular]{LiberationSans}
\renewcommand{\familydefault}{\sfdefault}

\usepackage{multicol}
\setlength{\columnsep}{1cm}

\usepackage{authblk}
\renewcommand\Affilfont{\small} % set font for affiliations

\input{caltech-style}  
\usetheme{Simple}
\usecolorstyle{Caltech}

\usepackage{physics}
\usepackage{dsfont}
\usepackage{graphicx}
\graphicspath{{./figures/}}


\newcommand{\oper}{\mathbf}

\usepackage[bibencoding=utf8,giveninits=true,sorting=none,maxcitenames=2,doi=false,url=false,isbn=false,]{biblatex}
\addbibresource{biblio.bib}
\AtBeginBibliography{\footnotesize} % set font for references

\title{Undecidability of the Spectral Gap in One Dimension}
\author[1]{Johannes Bausch}
\affil[1]{University of Cambridge, UK}
\author[2]{Toby S.\ Cubitt}
\affil[2]{University College London, UK}
\author[3,4]{\underline{Angelo Lucia}}
\affil[3]{QMATH \& NBIA, University of Copenhagen, DK}
\affil[4]{IQIM, Caltech, USA}
\author[5]{David~Perez-Garcia}
\affil[5]{Universidad Complutense de Madrid \& ICMAT, ES}
\institute{\texttt{arXiv:1810.01858}}

\usepackage[nolinks]{qrcode}
\titlegraphic{%
  \qrcode[tight,height=3cm]{https://angelolucia.xyz/}
\\ \vspace{1.5em}
\includegraphics[width=5cm]{caltech-logo.png} 
}

\begin{document}
\maketitle[roundedcorners=0]
 
\begin{columns}
  \column{0.5}
  \block{The spectral gap problem in 1D \dots}
  {
    A 1D, translation invariant, nearest-neighbor Hamiltonian on a $N$ spins
    chain is
    given by
    \[ \oper{H}_N = \sum_{i=1}^{N-1} \oper{h}^{(2)}_{i,i+1} + \sum_{i=1}^N
      \oper{h}^{(1)}_{i} \]
    where $\oper{h}^{(1)}$ (resp. $\oper{h}^{(2)}$) is a $d\times d$ (resp.
    $d^2\times d^2$) Hermitian matrix.


      The \textbf{spectral gap} $\Delta(\oper{H}_N)$
      of $\oper{H}_N$ is the difference between the two lowest distinct eigenvalues of
      $\oper{H}_N$.
    
      \innerblock[titleoffsety=0.5cm]{}{
      We say that $(\oper{h}^{(1)}, \oper{h}^{(2)})$ define a \textbf{gapped}
      Hamiltonian if $\oper{H}_N$ has a unique ground state and $\Delta(\oper{H}_N) > \gamma >
      0$ for all $N$.

      We say it is \textbf{gapless} if the spectrum of $\oper{H}_N$ above the
      groundstate becomes dense as $N$ goes to infinity.
    }

    \vspace{0.5em}
    Can we design an algorithm which takes $(\oper{h}^{(1)}, \oper{h}^{(2)})$
  as inputs and can tell us whether the Hamiltonian it defines is gapped or gapless?
  
    {\small These are ``strong'' notions of gapped/gapless. If we cannot
      distinguish between these two, we cannot distinguish the intermediate cases either.}
    \vspace{-1em}
}
\column{0.5}
\block{\dots\ is undecidable!}
{

  \innerblock[titleoffsety=0.5cm]{}{
  Given a Universal Turing Machine $M$ and a natural number $\eta$, we construct
  couplings $\oper{h}^{(1)}(\eta)$ and $\oper{h^{(2)}}(\eta)$ such that
  \begin{enumerate}
  \item if $M$ halts on input $\eta$, then  $\{\oper{H}_N\}_N$ is \emph{gapless};
  \item if $M$ does not halt on input $\eta$, then $\{\oper{H}_N\}_N$ is \emph{gapped}.
  \end{enumerate}
  }
  \vspace{0.5em}
  The Halting problem is undecidable~\cite{Turing1937}, which implies that the
  1D spectral gap is undecidable too. 

  The dependence on $\eta$ is only in weights and phases of the interaction terms:
  \begin{align*}
    \oper{h}^{(1)}(\eta) &= \oper{a} + \beta [ w(\eta) \oper{a}' + \oper{a}''] \\
    \oper{h}^{(2)}(\eta) &= \oper{b} + \beta [ w(\eta) \oper{b}' + \oper{b}'' + e^{i\pi \phi(\eta)} \oper{b}''' + e^{i\pi w(\eta)} \oper{b}'''' + \text{h.c.}]
  \end{align*}
  where $\oper{a}, \oper{b}$ are \textbf{classical} coupling, $\beta> 0$ is
  arbitrary, $w(\eta) = 2^{-2\abs{\eta}}$, $\phi(\eta)\in(0,1)$ is a binary
  encoding of $\eta$ and the other operators have entries
  in $\mathds{Q}[\sqrt{2}]$.

  \coloredbox{\textbf{Note:} the local dimension $d$ is fixed but extremely
    large.}
      \vspace{-1em}
}


\end{columns}

\begin{columns}
  \column{0.33}

  \block{Why is 1D important?}{

    We already knew that the spectral gap is undecidable in 2D systems
    \cite{Cubitt2015}. Why we care about the 1D result?
    \vspace{0.5em}
    \innerblock{Some 1D systems are hard!}{
      \begin{itemize}
        \item Approximating 1D ground state energy is
          QMA-hard~\cite{Kitaev2002,Aharonov2009}
        \item even with translationally-invariant nearest neighbour
          interactions~\cite{gottesman2009quantum}
        \item But: these hard instances are necessarily \textbf{gapless}
          systems~\cite{1703.10133, 1810.06528}
        \end{itemize}
      }
      \vspace{0.5em}
      \innerblock{Some 1D systems are easy!}{
      \begin{itemize}
      \item Many exactly solvable models
      \item No topological order~\cite{Verstraete2005}, no thermal phase transitions~\cite{Imry1974}
      \item Successful algorithms (DMRG)
      \item Matrix product state approximations
      \item \textbf{Provably} efficient algorithms for ground state energy of
        \textbf{gapped} systems~\cite{Landau2013}
        \end{itemize}
      }

      Because the spectral gap problem in undecidable, we cannot algorithmically distinguish
      easy from hard instances.
      \vspace{-2em}
    }

    \block{Why is it hard to prove this?}{

    The proof in 2D relies on an aperiodic tiling: the Robinson tiling. This is
    needed to have a ``fractal'' structure: a fixed density of structures at all length scales.
    \vspace{0.3em}
    \coloredbox{There are no aperiodic tilings in 1D!}

    Instead, we construct a \textbf{marker} Hamiltonian: it separates the spin
    chain in a \emph{periodic} partition, but whose length and period depend on
    the halting time of the Turing machine (which is also an undecidable
    quantity!).

    {\center
      \hspace{2em}
      \includegraphics[width=0.2\textwidth]{fig-segments}
      }
    }
    
  \column{0.33}



    \block{How did we prove it?}
    {
      Feynman and Kitaev’s history state
      construction~\cite{Kitaev2002}: a Hamiltonian whose ground state energy is dependent
      on the outcome of a (quantum) computation. For quantum
      Turing machines:~\textcite{gottesman2009quantum}.

      \vspace{1em}
      
      We use a quantum Turing machine which does Quantum Phase Estimation to
      read $\phi(\eta)$, and then simulates the universal (classical) Turing
      machine $M$ with input $\eta$.

      \vspace{1em}
      \coloredbox{These are necessarily gapless Hamiltonians.}
      {\center
        \includegraphics[width=0.27\textwidth]{energy-penalty-and-bonus}
      }

      We run one instance of the QTM on each segment of the chain determined by
      the marker Hamiltonian.
      
      \begin{enumerate}
        \item If $M(\eta)$ \textbf{does not} halt, it will always run out of
          tape. The most energetically favorable configuration is one single
          instance of the Turing machine on the longest segment possible.
        \item If $M(\eta)$ \textbf{does} halt, there is an optimal tape
          length. The most energetically favorable configuration is having many
          copies of the Turing machine on these segment length.
        \end{enumerate}

        In case 1., the g.s. energy tends to $0$. In case 2., it diverges to
        $-\infty$ (constant negative density). This can be used to
        construct a \textbf{switch}:

        \[ ( \oper{H}_{\text{QTM}} + \oper{H}_{\text{marker}} )\otimes \oper{H}_{\text{dense}}
            + \oper{H}_{\text{gapped}} + \oper{H}_{\text{switch}} \] 
        $ \oper{H}_{\text{switch}}$ makes sure that the low-energy spectrum is either
        the one of $\oper{H}_{\text{dense}}$ or of $\oper{H}_{\text{gapped}}$, depending on
        whether the QTM Hamiltonian has 0 or negative g.s. energy.
    }
    

  \column{0.33}

  \block{Frequently Asked Questions}{
    \begin{description}
      \item[Is this generic?] It depends on the distribution. For
        example, generic \emph{disordered} Hamiltonians are gapless in
        1D~\cite{Movassagh2017}, but gapped if they are translation invariant~\cite{Lemm2019}.
      \item[Is this stable?] For arbitrary perturbations, probably not, but the
        spectral gap in general is not always stable (and it is a hard problem
        even for simpler models). Small perturbations of $\eta$ can change the
        instance from gapped to gapless (or vice versa).
        Even classical interactions are not stable: take $\beta$ small!
      \item[Are ground states entangled?] Not necessarily, but they can be. In
        the halting (gapless) case, all the terms except from
        $\oper{H}_{\text{dense}}$ contribute little to the groundstate
        entanglement. By choosing it properly, one can have both finite (but \textbf{uncomputably large})
        or diverging entanglement. 
        
      \end{description}
      \vspace{-2em}
  }
  
  \block{What's next?}{
      \textbf{The local dimension:} we know that frustration-free \textbf{qubit}
        ($d=2$) chains are \textbf{decidable}~\cite{Bravyi2015}. Is there a local dimension
        threshold before which the problem is always decidable?

        \vspace{1em}
        
   \innerblock{References}{
      \printbibliography[heading=none]
  }

  }
  
\end{columns}

\end{document}
